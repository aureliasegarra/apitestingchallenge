[![Front](https://img.shields.io/badge/Test-APITesting-green?style=flat)](https://)
[![Front](https://img.shields.io/badge/Challenge-MinistryOfTesting-black?style=flat)](https://)


<div align="center">
    <h1>API Testing Challenge</h1>
    <img src="images/logoMOT.png" alt="Logo Ministry of testing" width="10%">
</div>
<br>
<br>

This work is based on The 30 Days of Testing Challenge by Ministry of Testing.

Work in progress ...

[See challenge](https://www.ministryoftesting.com/dojo/lessons/30-days-of-api-testing)
<br>
[30 Days of API Testing Challenge (PDF)](pdf/30_Days_of_API_TestingV2.pdf)

<details close="close">
  <summary>☑️ 1 - How to define API Testing </summary>
Types of software testing that involves application programming interfaces.

Important step in the software development life cycle

* testing an API

* testing its integration with the services
</details>

<details close="close">
  <summary>☑️ 2 - How approach API Exploring Testing</summary>
Test cases are not created in advance but testers check the system on the fly.
It’s all about discovery, investigation and learning.
 
* Exploring the product to understand it, what works, the behaviour of the system.

#### Type of API

* Read the documenation of the API !

#### Create initial Explore plan

#### Http codes and error codes handling

#### What type of API request and response - endpoint

#### Get a tool to perform the exploratory testing

#### Documenting your staff while exploring APIs
</details>

<details close="close">
  <summary>☑️ 3 - Top Books on API Testing </summary>

<div style="display: flex">
<br>
  <img src="images/alanRichardson.png" alt="Book's illustration" width="30%" style="margin-right: 10px">

<br>
  <img src="images/jonathanRasmusson.jpeg" alt="Book's illustration" width="30%" style="margin-right: 10px">

<br>
  <img src="images/rahulShetty.jpeg" alt="Book's illustration" width="30%" style="margin-right: 10px">

</div>

* Learn video https://scrolltest.com/api/day3
</details>



<details close="close">
  <summary>☑️ 4 - How does HTTP work </summary>

#### How it works
* Client/Server Protocol which allows to fetch resources
* Foundation of any data exchange on the web
* Application Layer Protocol sent over TCP 
* Request => Message SENT by the client
* Response => Message SENT by the server

#### Basic aspect of HTTP
* Simple and human readable
* HTTP/2 (more secure) encapsulate HTTP messages into frames
* HTTP is stateless (no link between 2 requests being successively 
carried out on the same connection)
* HTTP is not sessionless

#### HTTP Flow
* A client wants to communicate with a server
* Open a TCP connection
* Send an HTTP message
  ![http request illustration](images/http_request.png)
*https://developer.mozilla.org/fr/docs/Web/HTTP/Overview*

* Read the response sent by the server
  ![http response illustration](images/http_response.png)
*https://developer.mozilla.org/fr/docs/Web/HTTP/Overview*
</details>

<details close="close">
  <summary>☑️ 5 - Public API to play with </summary>

* [jsonplaceholder.typicode.com](https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbHJxbTI1bzNBdmpnRnRLWjd4cDV2bWpzMXhvZ3xBQ3Jtc0tuNnQxOEZBODBWeWZIb2pudHN0ZmY5SVlyVUxxXzNkTTdlOEoyS0g5RXpLbzBPNUcxdXBDdXFYWjFsVkozVVVQQUgzbmhkbEljeEhCcnM5NDNINVItQmlacHI0aDVqUFpXdlNKV1lnSm1DWlM2QU5uSQ&q=https%3A%2F%2Fjsonplaceholder.typicode.com%2F&v=WCwofnisiFY)
* [httpbin.org](http://httpbin.org)

<img src="images/GET_Request_postman.png">
</details>

<details close="close">
  <summary>☑️ 6 - Blogs about API Testing </summary>

* [https://www.one-tab.com/page/ZXKnBSB_...](https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbDdoZFlyT3I1LUZoWGgzcXFwaTRaMmhlTlktQXxBQ3Jtc0trRldTTmhoRW5iT2hwT3VpTG1xRmpHZjRRdmlZRVJ4SG5HdUJEbnBCVnFHenVoM0JfTm11NGF0UXJUVGZ0cVhXdHVZTVpRdDBoMU5vWVU2dFZjSlVzY1FES1lZRVFqNHpXbmk0dk14NFgtWXZPSFJIMA&q=https%3A%2F%2Fwww.one-tab.com%2Fpage%2FZXKnBSB_TZWBj3-ReGqCWA&v=1iD9hqqKFgc)
* Postman blog 
* Parasoft blog 
* Katalon blog 
* QA source
* Blog test project
</details>

<details close="close">
  <summary>☑️ 7 - GET and POST request </summary>

#### Tasks
Execute some simple API calls using Postman
#### Objectives
* Send a GET and POST request
* Improve Postman by create workspace and collection
#### Exercices
* GET call on https://automationtesting.online/room
![postman illustration](images/GET_request.png)
* POST call on https://automationtesting.online/auth/login to get a token
```
{
  "username": "admin",
  "password": "password"
}
```
![postman illustration](images/POST_request.png)
</details>

<details close="close">
  <summary>☑️ 8 - Test cases REST API </summary>

#### Some tasks : Test cases on REST API
* HTTP status code. 
* Valid Response payload 
* Response Time Parse the Response data 
* Valid Response headers
* Negative Testcases response 
* Security Related Test Cases 
* Schema validation 
* Field Type 
* Mandatory Fields 
* Chaining Request verification
* Verification of APIs with Data parameters
* End to End CRUD flows 
* Database Integrity Test Cases 
* File Upload Testcases

Link to my workspace in Postman : https://www.postman.com/aureliacbien/workspace/apitesting

</details>

<details close="close">
  <summary>☑️ 9 - Tools to discover API calls </summary>

#### Tools
* Dev tools (Network section)
* Charles Proxy (https://www.charlesproxy.com)
* Fiddler for Win (https://www.telerik.com/fiddler)

</details>

<details close="close">
  <summary>☑️ 10 - Why Postman ? </summary>

#### Best Features for me
* Testing request (Rest, SOAP, GraphQ, testing headers, authentication ...)
* Testing response (mock response, cookies ...)
* Provides variables (differents environments)
* Scripting (to send requests, test suit ...)
* Collaboration for teams
* Structure (Collections)
* CI (Newman)
* Mock server
* Easy to use

</details>

<details close="close">
  <summary>☑️ 11 - API Testing Tutorials </summary>

#### Types of APIs
* Open APIs (ex: openweathermap)
* Partner APIs
* Internal APIs
* Composite APIs

#### Web Service APIs
* **REST**
* SOAP
* XML
* JSON
</details>

<details close="close">
  <summary>☑️ 12 - Before Learning API testing </summary>

#### For Postman
* Javascript
* Assertion and shema validation
* Chai JS basics
* BDD

#### Tools
* Chrome Dev tools
* Jenkins CI/CD tools
* Docker / Kubernetes
* Web Proxy Tools (Fiddler/Charles)

#### Essentials
* JSON XML Parsing
* HTTP Methods
* Be able to create test scenario, test cases
* Exploratory testing
* Request, response, headers, authentication, cookies, sessions
* API types
* Database skills (SQL basics)

</details>